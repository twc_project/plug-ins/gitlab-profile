# Plug-ins for a Modular Compositor

The repositories in this group contain compositors and plug-ins based
on a modular Wayland implementation called [libtwc].  See the [TWC
Project] for more information.

The repositories are:

  - [smpl]

    A simple example of a Built-in Client.

  - [ctwc]

    A collection of plug-ins which provide a CTWM work-alike for
    Wayland.

  - [tmpl]

    A template plug-in to serve as a starting point for new modules.
    It is more complete than smpl.

  - [test0]

    A module for trying ideas.

In addition to dynamic plug-ins, each module is also built as an
archive library which may be statically linked with a modular
compositor.

[TWC Project]: https://gitlab.com/twc_project
[libtwc]:      https://gitlab.com/twc_project/libtwc
[smpl]:        https://gitlab.com/twc_project/plug-ins/smpl
[ctwc]:        https://gitlab.com/twc_project/plug-ins/ctwc
[tmpl]:        https://gitlab.com/twc_project/plug-ins/tmpl
[test0]:       https://gitlab.com/twc_project/plug-ins/test0
